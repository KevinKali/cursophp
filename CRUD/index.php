<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CRUD</title>
<link rel="stylesheet" type="text/css" href="hoja.css">
</head>

<body>

<?php
    include("conecction.php");

    if(isset($_POST['cr'])){
      $sql_insert = "insert into crud1(nombre,edad) values (:name,:edad)";

      $result = $con->prepare($sql_insert);
      $result->execute(array(':name'=>$_POST['Nom'], ':edad'=>$_POST['edad']));

      header('location:index.php');
    }

    $sql = "select * from crud1";
    $result = $con->query($sql)->fetchAll(PDO::FETCH_OBJ);
    // var_dump($result);
?>

  <h1>CRUD<span class="subtitulo">Create Read Update Delete</span></h1>
  
  <table width="50%" border="0" align="center">
    <tr >
      <td class="primera_fila">Id</td>
      <td class="primera_fila">Nombre</td>
      <td class="primera_fila">Edad</td>
      <td class="sin">&nbsp;</td>
      <td class="sin">&nbsp;</td>
      <td class="sin">&nbsp;</td>
    </tr> 

		<?php foreach($result as $i): ?>
   	<tr>
      <td><?php echo $i->id ?></td>
      <td><?php echo $i->nombre ?></td>
      <td><?php echo $i->edad ?></td>
 
      <td class="bot"><a href="delete.php?id=<?php echo $i->id ?>"><input type='button' name='del' id='del' value='Borrar'></a></td>
      <td class='bot'><a href="editar.php?id=<?php echo $i->id ?>"><input type='button' name='up' id='up' value='Actualizar'></a></a></td>
    </tr> 
    <?php endforeach; ?>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
      <tr>
      <td></td>
          <td><input type='text' name='Nom' size='10' class='centrado'></td>
          <td><input type='number' name='edad' size='10' class='centrado'></td>
          <td class='bot'><input type='submit' name='cr' id='cr' value='Insertar'></td>
      </tr>   
    </form>

    </table>

<p>&nbsp;</p>
</body>
</html>