<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        try{
            $base = new PDO("mysql:host=localhost; dbname=prueba","php","201299Kei@");
            $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = "insert into user_pass(user,password) values (:login,:password);";

            $result = $base->prepare($sql);

            $login=htmlentities(addslashes($_POST['login'])); // convertir cualquier simbolo en html, para evitar inyeccion sql
            $password = htmlentities(addslashes($_POST['password']));
            
            $login_cifrado = password_hash($password,PASSWORD_DEFAULT,array("cost"=>12));

            $result->bindValue(":login",$login);
            $result->bindValue(":password",$login_cifrado);
            $result->execute();

            $result->closeCursor();
        }catch(Exception $e){
            die("Error: " . $e->getMessage());
        }finally{
            $result = null;
            $base = null;
        }
    ?>
</body>
</html>