
<?php require('reserva.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $booksJSON = file_get_contents('./books.json');
        
        $books = json_decode($booksJSON,true);
        // var_dump($books);
        if(isset($_GET['title'])){
            echo '<p>Looking for <b>' . $_GET['title'] . '</p>';
            if (booking($books, $_GET['title'])) {
                echo 'Booked!';
                update($books);
            } else {
                echo 'The book is not available...';
            }
        }else{
            echo '<p>You are not looking for a book?</p>';
        }
    ?>

    <ul>
        <?php foreach($books as $book): ?>
            <li>
                <a href="?title=<?php echo $book['title']; ?>">
                    <?php echo $book['title']; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</body>
</html>