<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        session_start();

        // si no hay nada almacenado en sesion hacer...
        if(!isset($_SESSION['user'])){
            header("location:index.html");
        }
    ?>

    <h1>This information is for only login users</h1>
    <a href="./user_register.php">volver</a> <a href="./log_out.php">Cerrar sesion</a>
    
</body>
</html>