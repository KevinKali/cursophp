<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        session_start();

        // si no hay nada almacenado en sesion hacer...
        if(!isset($_SESSION['user'])){
            header("location:index.html");
        }
    ?>

    <h1>This information is for only login users</h1>

    <table>
        <thead>
        <tr>
            <td colspan="2" style="text-align:center;">zona usuarios registrados</td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href="./user_register2.php">Pagina1</a></td>
                <td><a href="./user_register3.php">Pagina2</a></td>
            </tr>
        </tbody>
    </table>
</body>
</html>