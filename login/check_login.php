<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        try{
            $base = new PDO("mysql:host=localhost; dbname=prueba","php","201299Kei@");
            $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = "select * from user_pass where user like :login and password like :password";

            $result = $base->prepare($sql);

            $login=htmlentities(addslashes($_POST['login'])); // convertir cualquier simbolo en html, para evitar inyeccion sql
            $password = htmlentities(addslashes($_POST['password']));

            $result->bindValue(":login",$login);
            $result->bindValue(":password",$password);
            $result->execute();

            if($result->rowCount()){

                // antes te redirigir al usuario, iniciar sesion, para validar que ha realizado, satisfactoriamente la sesion
                session_start();
                $_SESSION['user']=$_POST['login'];

                header("location:user_register.php");
            }else{
                header("location:index.html");
            }

            $result->closeCursor();
        }catch(Exception $e){
            die("Error: " . $e->getMessage());
        }finally{
            $result = null;
            $base = null;
        }
    ?>
</body>
</html>