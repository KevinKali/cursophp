<!-- Consultas preparadas con PDO y parametrizada-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <?php
        $nombre = $_GET['name'];
        $edad = $_GET['edad'];

        try{
            $base = new PDO('mysql:host=localhost; dbname=prueba', 'php', '201299kei');
            $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $base->exec('SET CHARACTER SET utf8');

            $sql = "select * from datospersonales where nombre like :name and edad = :age";

            $result = $base->prepare($sql);
            $result->execute(array(":name"=>$nombre, ":age"=>$edad));
            
            while($data = $result->fetch(PDO::FETCH_ASSOC)){
                echo $data['nif'] . "   " . $data['nombre'] . "  " . $data['edad'] . "<br>";
            }

            if($result->rowCount() > 0){
                echo "<br>tuani<br>";
            }else{
                echo "<br>huy<br>";
            }

            $result->closeCursor();

            echo "Conexion tuani";
        }catch(Exception $e){
            die("Error: " . $e->GetMessage());
        }finally{
            $base = null;
        }
    ?>
    
</body>
</html>