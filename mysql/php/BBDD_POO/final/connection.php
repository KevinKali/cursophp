<?php

require "conf.php";

class Connection_BD_Kali{
    protected $db_con;

    public function Connection_BD_Kali(){
        $this->db_con = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        if($this->db_con->connect_errno){
            echo "Fallo al conectar a Mysql: " . $this->db_con->connect_error;
            return;
        }
        $this->db_con->set_charset(DB_CHARSET);
    }
}