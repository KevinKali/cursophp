<?php

require "connection.php";

class Persona extends Connection_BD_Kali{
    public function Person(){
        parent::__construct();
    }

    public function getPersonas(){
        $resultado = $this->db_con->query('select * from datospersonales');
        $personas = $resultado->fetch_all(MYSQLI_ASSOC);

        return $personas;
    }
}