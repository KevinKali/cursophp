<?php
    require ("./BBDD_POO/final/persona.php");

    $personas = new Persona();
    $lista = $personas->getPersonas();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        foreach($lista as $i){
            echo $i['nif'] . "  " . $i['nombre'] . "  " . $i['apellido'] . "<br>";
        }
    ?>
</body>
</html>