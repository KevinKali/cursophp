<?php 
    /*
        direccion de db
        nombre de db
        usuario de db
        contraseña de db

        para la realidad se necesita un espacio web con php y bd de mysql
    */

    //modos de conexion
    //orientado a objetos hacer clase mysqli

    //por procedimientos funcion mysqli_connect

    //conexion orientada a objetos
    require('./datos_conexion.php');

    // $conexion = mysqli_connect($db_host, $db_user, $db_password, $db_name);
    $conexion=mysqli_connect($db_host, $db_user, $db_password,"");

    //si hay un error de conexion 
    if(mysqli_connect_errno()){
        echo "Fallo de conexion de db";
        exit();
    }

    //dejar una base de datos por defecto y luego asignarla
    mysqli_select_db($conexion, $db_name) or die ("no se encuentra la database");

    //el resultado obtenido sera en formato utf-8
    mysqli_set_charset($conexion, "utf-8");

    $consulta = "SELECT D.NOMBRE, D.APELLIDO, D.EDAD FROM DATOSPERSONALES D";

    $resultado = mysqli_query($conexion, $consulta);

    //el resultado obtenido sera por el valor asociado a cada columna de la fila
    while($row = $resultado->fetch_assoc()) {
        echo "nombre: " . $row["NOMBRE"];
    }

    $consulta = "SELECT * FROM prueba.artículos";
    $resultado = mysqli_query($conexion, $consulta);

    while($row = $resultado->fetch_assoc()){
        echo "nombre: " . $row["E"] . "";
        echo "<br>";
    }

    //cerrar la conexion
    mysqli_close($conexion);
?>