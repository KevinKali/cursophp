<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            background-color: pink;
        }
    </style>
</head>
<body>

<?php 
        print "<h1>e </h1>sda a <br>";
        print "e el a a <br>";
        //Los comentarios se hacen de esta manera o
        /*De esta manera*/

        //variables
        $nombre = "kevin";
        $numero=2;

        //para concatenar usar el operador punto
        print "El nombre del user es: " . $nombre;
        print "<br>El nombre del user es $nombre <br>";
        print "El numero es: " . $numero;

        echo "<br> tuani todo: $nombre";
        echo "<br> tuani todo: " . $nombre . " Y la edad es: " . $numero;
        echo "<br><br> En fin el nombre es: $nombre y la edad es $numero";

        echo "<br>";
        echo $nombre, $numero;
    ?>

    <?php 

        echo "es posible tenemos muchos php embedidos";

        //include("proporciona_Datos.php");         //incluyendo datos en archivos php
        require("proporciona_Datos.php");           //se puede incluir tambien con la sentencia require

        // include y require tiene una diferencia, cuando se usa include quiere decirle al web que aunque no encuentre el archivo incluido aun corra las demas instruccciones que aparecen en el flujo de codigo de php. Cuando es require si el archivo requerido no existe no continua el flujo de datos del programa y habra problemas

        helloWorld();   //llamando a funcion de php
    ?>
</body>
</html>