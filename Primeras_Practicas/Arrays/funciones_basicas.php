<?php 
    //muestro el array numerico original
$array1 = array(1,2,3,4,5,6);
var_export ($array1);
 
//elimino el tercer elemento del array y muestro el array
unset($array1[2]);
var_export ($array1);
 
//elimino el primer y segundo elemento del array y muestro el array
unset($array1[0],$array1[1]);
var_export ($array1);







//Declaro el array asociativo y muestro su valor
$array2 = array(uno=>1, dos=>2, tres=>3, cuatro=>4, cinco=>5, seis=>6);
var_export ($array2);
 
//Elimino el elemento con clave = dos y muestro el array
unset($array2['dos']);
var_export ($array2);
 
//Elimino los elementos con clave = uno y cinco, despues muestro el array
unset($array2['uno'],$array2['cinco']);
var_export ($array2);
?>