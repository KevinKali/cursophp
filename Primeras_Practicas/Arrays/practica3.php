<?php

    //los arrays multidimensionales en php pueden ser de diferentes tamaños en filas y columnas
    $alimentos = array(
        "fruta"=>array("tropical"=>"kiwi", 
                        "citrico"=>"mandarina",
                        "otros"=>"manzana"),
        "leche"=>array("animal"=>"vaca",
                        "vegetal"=>"coco"),
        "carne"=>array("vacuno"=>"lomo",
                        "porcino"=>"pata"));


    echo $alimentos["carne"]["vacuno"];

    echo "<br>";
    
    //--recorriendo todo el array
    foreach($alimentos as $clave_alim=>$alim){
        
        echo "<br>$clave_alim:<br>";

        //mientras hayan elementos en la lista hacer por cada elemento hacer...
        while(list($clave,$valor)=each($alim)){
            echo "$clave = $valor<br>";
        }
    }
    echo "<br>";

    //--------------------------------------------------------

    $fruits[0][0]=1;
    $fruits[0][1]=2;
    $fruits[1][0]=3;
    $fruits[1][1]=4;

    for($i=0; $i<count($fruits); $i++){
        for ($j=0; $j<count($fruits); $j++)
            echo $fruits[$i][$j] . "           ";

        echo "<br>";
    }
?>