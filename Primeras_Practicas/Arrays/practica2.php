<?php 
    $datos = array("nombre"=>"Kevin", "apellido"=>"Gómez","Edad"=>19);

    if(is_array($datos)){
        echo "<br>It´s Ok<br>";
    }else{
        echo "<br>Sorry<br>";
    }

    $datos="Josue";     //la variable deja de ser arreglo para ser string

    //comprobar si una variable es array o variable

    if(is_array($datos)){
        echo "<br>" . $datos["nombre"] . "<br>";
    }else{
        echo $datos;
    }
    echo "<br>";
    echo "<br>";

    $semana=array("a"=>1, "b"=>2, "c"=>3, "d"=>4);

    //para recorrer arrays asociativos usar foreach
    foreach($semana as $clave=>$valor){
        echo "$clave = $valor <br>";
    }
    echo "<br>";
    echo "<br>";

    //----------------------------Agregar elemento a un array asociativo---------------------------

    $semana["país"]="Nicaragua";
    foreach($semana as $clave=>$valor){
        echo "$clave = $valor <br>";
    }
    echo "<br>";
    echo "<br>";

    //-------------------------------------------------------
    $semana2[]="Lunes";
    $semana2[]="Martes";
    $semana2[]="Miercoles";
    $semana2[]="Jueves";
    $semana2[]="Viernes";

    for($i=0; $i<count($semana2); $i++){
        echo $semana2[$i] . "<br>";
    }
    echo "<br>";
    echo "<br>";

    //para agregar un elemento sin saber en que indice pues lo hacemos sin especificar el indice
    $semana2[]="Sabado";
    for($i=0; $i<count($semana2); $i++){
        echo $semana2[$i] . "<br>";
    }
    echo "<br>";
    echo "<br>";

    sort($semana2);
    for($i=0; $i<count($semana2); $i++){
        echo $semana2[$i] . "<br>";
    }
?>