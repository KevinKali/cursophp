<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>&nbsp;</p>
    <form name="form1" method="post">
    <p>
        <label for="num1"></label>
        <input type="text" name="num1" id="num1"/>
        <label for="num2"></label>
        <input type="text" name="num2" id="num2"/>
        <label for="operacion"></label>
        <select name="operacion" id="operacion">
            <option value="Suma">Suma</option>
            <option value="Resta">Resta</option>
            <option value="Multiplicacion">Multiplicación</option>
            <option value="Division">División</option>
            <option value="Modulo">Módulo</option>
            <option value="incrementa">Incrementar</option>
            <option value="decrementa">Decrementar</option>
        </select>
    </p>
    <p>
        <input type="submit" value="Enviar" name="button" onClick="prueba" id="button">
    </p>
    </form>
    <p>&nbsp;</p>

    <?php 
        include("calculadora.php");

        if(isset($_POST["button"])){
            $num1 = $_POST["num1"];
            $num2 = $_POST["num2"];
            $operacion = $_POST["operacion"];

            calcular($num1, $num2,$operacion);
        }
    ?>
</body>
</html>