<?php 
    function incrementeaVariable(){
        $contador = 0;

        $contador++;

        echo $contador . "<br>";
    }

    //La variable no cambia cuando se llama de nuevo el valor contador, debido a que no es estatica
    incrementeaVariable();
    incrementeaVariable();
    incrementeaVariable();

    function static_variable(){
        static $contador =0;

        $contador++;

        echo "<br>" . $contador;
    }

    static_variable();
    static_variable();
    static_variable();
    static_variable();
?>