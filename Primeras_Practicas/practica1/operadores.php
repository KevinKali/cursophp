<?php 
    $var1 = 8;
    $var2 = "8";
    $var3 = "Kevin";

    if($var1 == $var2){
        echo "Son iguales";
    }else{
        echo "No son iguales";
    }

    echo "<br><br>";
    if($var1 == $var3){
        echo "Son iguales";
    }else{
        echo "No son iguales";
    }
    

    echo "<br><br>";
    if($var1 === $var2){        //no son iguales porque evalua el tipo de dato tambien
        echo "Son iguales";
    }else{
        echo "No son iguales";
    }

    echo "<br><br>";
    if($var1 != $var2){
        echo "Son diferentes";
    }else{
        echo "No son diferentes";
    }

    echo "<br><br>";
    if($var1 != $var3){
        echo "Son diferentes";
    }else{
        echo "No son diferentes";
    }

    echo "<br><br>";
    if($var1 <> $var2){
        echo "Son diferentes";
    }else{
        echo "No son diferentes";
    }

    //tambien hay valores > y < evaluando numeros, >= y <=
?>