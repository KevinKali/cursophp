<?php 
    $nombre="kevin";

    //la variable nombre que tiene kevin es diferente a la que tiene cristina son distintos
    function giveName(){
        $nombre = "Cristina";   //no accede a la var que esta fuera.
    }

    function globalName(){
        global $nombre;     //accedemos al control de una variable fuera de la funcion
        $nombre = "El nombre es $nombre";
    }

    giveName();
    globalName();

    echo $nombre;
?>