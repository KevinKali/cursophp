<style>
.string{
    font-size: 30px;
    font-weight: bold;
}
</style>

<?php 
    echo "<p class='string'>concatenando con comillas <p>";
    echo "<p class=\"string\">con barra invertida <p>";

    $nombre="kevin";

    echo "Hola $nombre";
    echo '<br>Hola $nombre';

    echo "<br>";
    echo "Comparando string";
    echo "<br>";

    $var1 = "casa";
    $var2 = "CASA";

    $resultado = strcmp($var1,$var2); //devuelve 1 si no es igual y 0 si es 
    echo $resultado;

    echo "<br>";
    $resultado = strcasecmp($var1,$var2); //devuelve 1 si no es igual y 0 si es 
    echo $resultado;

    echo "<br>";
    echo "<br>";
    if(!$resultado){    //0 coincide
        echo "El resultado es verdadero";
    }else{
        echo "El resultado es negativo";
    }
?>