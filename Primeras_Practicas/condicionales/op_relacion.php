<?php 
    $var1 = true;
    $var2 = false;

    $resultado = $var1 && $var2; // resultado = false

    if($resultado == true){
        echo "Correct";
    }else{
        echo "Incorrecto";
    }

    echo "<br>";
    $resultado = $var1 and $var2; // resultado = false

    if($resultado == true){
        echo "Correct";
    }else{
        echo "Incorrecto";
    }

    //con el || y or es igual
    //y la negacion con !
?>