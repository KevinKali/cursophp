<?php 
    $var1=1;

    while($var1<6){
        echo "<br>" . $var1++;
    }
    echo "<br><br> Termino while <br><br>";

    do{
        echo "<br>" . $var1++;
    }while($var1<=10);
    echo "<br><br> Termino Do_While <br><br>";

    for($i=$var1; $i<=15; $i++){
        echo "<br>" . $i;
    }
    echo "<br><br> Termino For <br><br>";

    //se puede hacer uso de continue y break al igual que cualquier otro lenguaje de programacion
?>