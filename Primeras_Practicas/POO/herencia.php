<?php 
    class Coche{
        var $rueda;
        var $color;

        //metodo constructor
        function Coche(){
            $this->rueda = 4;
            $this->color = "pink";
        }

        function frenar(){
            echo "<br>estoy frenando";
        }

        function tuani(){
            return "<br>tuani";
        }

        function set_color($color){
            $this->color = $color;
        }
    }

    class Camion extends Coche{
        function Camion($color){
            $this->rueda=8;
            $this->color=$color;
        }

        //override
        function set_color($color){
            $this->color="green";
        }

        function jugar(){
            echo "<br>Jugar";
        }

        // Llamar al metodo de la clase padre
        function tuani(){
            echo "" . parent::tuani();    //ejecute todo el metodo de la clase padre

            echo "<br>Esto esta super tuani";
        }
    }

    $pegaso = new Camion("pink");
    echo $pegaso->color;
    $pegaso->set_color("red");
    echo $pegaso->color;

    echo $pegaso->frenar();

    echo $pegaso->tuani();
?>