<!-- Buscar en internet listado de funciones php -->
<!-- para conocer las predefinidas por el lenguaje -->
<?php
    echo "Tuani convertido en mayuscula es: " . (strtoupper("tuani"));

    // Creadas por el user
    function suma($var1, $var2){
        $resultado = $var1 + $var2;

        return $resultado;
    }

    $result = suma(2,5);

    echo "<br><br>" . $result;

    // Valor por defecto
    function fras_mays($frase, $conversion=true){
        $frase=strtolower($frase);

        if($conversion==true){
            $resultado =ucwords($frase);
        }else{
            $resultado = strtoupper($frase);
        }

        return $resultado;
    }

    $result=fras_mays("Hello",false);
    echo "<br><br>" . $result;
    $result=fras_mays("Hello");
    echo "<br><br>" . $result;
?>