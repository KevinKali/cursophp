<?php 
    // Parametros por valor o referencia
    function increment($v1){
        $v1++;

        return $v1;
    }
    echo increment(2);

    echo "<br><br>";

    $numero = 5;
    echo increment($numero);

    echo "<br><br>";
    // Argumento por referencia
    function incrementa(&$v1){
        $v1++;

        return $v1;
    }
    $numero = 10;
    echo incrementa($numero);
    echo "<br><br>";
    echo $numero;
?>