<?php 
    class Coche{

        var $tuani1;        //var global y normal
        private $tuani2;    //acceso privado
        protected $rueda;
        protected $color;   //acceso mismo y herencia

        //metodo constructor
        function Coche(){
            $this->rueda = 4;
            $this->color = "pink";
        }

        function frenar(){
            echo "estoy frenando";
        }

        function tuani(){
            return "tuani";
        }

        //cuando se trabaja con modificadores de datos
        //es necesario usar setter y getter para las variables
        //privadas y protected
        function set_color($color){
            $this->color = $color;
        }

        function getRueda(){
            return $this->rueda;
        }
    }

    class Camion extends Coche{
        function Camion($color){
            $this->rueda=8;
            $this->color=$color;
        }

        //override
        function set_color($color){
            $this->color="green";
        }

        function jugar(){
            echo "<br>Jugar";
        }

        // Llamar al metodo de la clase padre
        function tuani(){
            echo "" . parent::tuani();    //ejecute todo el metodo de la clase padre

            echo "<br>Esto esta super tuani";
        }
    }

    $tuani = new Coche();
    $camion = new Camion("blue");
    //no se puede acceder a propiedades privadas fuera de la clase
    //$tuani->rueda=2;

    echo $tuani->getRueda();
    echo $camion->getRueda();

?>