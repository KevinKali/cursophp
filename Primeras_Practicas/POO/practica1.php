<?php 
    class Coche{
        var $rueda;
        var $color;

        //metodo constructor
        function Coche(){
            $this->rueda = 4;
            $this->color = "pink";
        }

        function frenar(){
            echo "estoy frenando";
        }

        function tuani(){
            return "tuani";
        }

        function set_color($color){
            $this->color = $color;
        }
    }

    $tuani = new Coche();

    $tuani->frenar();
    echo "<br>La cantidad de ruedas son: " . $tuani->rueda;
    echo "<br><br>" . $tuani->tuani() . " El color es: " . $tuani->color;

    $tuani->set_color("red");
    echo "<br><br>" . $tuani->tuani() . " El color es: " . $tuani->color;
?>