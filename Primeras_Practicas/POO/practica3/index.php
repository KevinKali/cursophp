<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>

<body>

<?php
    include("practica3.php");
    //haciendo referencia a una variable static de la clase Compra_Vehiculo
    Compra_vehiculo::$ayuda=10000;
    
    //Estamos accediendo a una variable estatica privada dara error
    //Compra_Vehiculo::$acceso_dentro_clase;

    Compra_Vehiculo::descuento();
    echo Compra_Vehiculo::get_acceso() . "<br>";
    
    //los objetos no pueden acceder a variables static
	$compra_Antonio=new Compra_vehiculo("compacto");
	$compra_Antonio->climatizador();$compra_Antonio->tapiceria_cuero("blanco");
    
    echo $compra_Antonio->precio_final() . "<br>";
    
	$compra_Ana=new Compra_vehiculo("compacto");
	$compra_Ana->climatizador();
	$compra_Ana->tapiceria_cuero("rojo");
	
	echo $compra_Ana->precio_final();
?>
</body>
</html>