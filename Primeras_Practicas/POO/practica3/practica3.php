<?php
	class Compra_Vehiculo{
		private $precio_base;
		static $ayuda=4500;

		//la variable static se puede encapsular
		private static $acceso_dentro_clase;
				
		function Compra_vehiculo($gama){
			if($gama=="urbano"){
					$this->precio_base=10000;	
			}else if($gama=="compacto"){
					$this->precio_base=20000;	
			}
			else if($gama=="berlina"){
					$this->precio_base=30000;	
			}		
		}// fin constructor
		
		static function descuento(){

			if(date("d-m-y") == "20-10-19"){
				self::$acceso_dentro_clase=500;
			}else{
				self::$acceso_dentro_clase=5000;
			}
		}

		static function get_acceso(){
			return self::$acceso_dentro_clase;
		}

		function climatizador(){
				$this->precio_base+=2000;
		}// fin climatizador
		
		function navegador_gps(){
			$this->precio_base+=2500;	
		}//fin navegador gps
		
		function tapiceria_cuero($color){
			if($color=="blanco"){
			
				$this->precio_base+=3000;
			}
			else if($color=="beige"){
				$this->precio_base+=3500;	
			}
			else{
				$this->precio_base+=5000;	
			}
		}// fin tapicería
		
		function precio_final(){
			//para hacer referencia a un campo static se usa self::nombre_var_static
			$valor_final = $this->precio_base - self::$ayuda;
			return $valor_final;

			// $valor_final=$this->precio_base-self::$ayuda;
		}// fin precio final
	}// fin clase
?>