<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        //cookie que cuando cierra el navegador se pierde
        // setcookie("tuani", "this is a cookie very good");

        // El identificador, el valor y el tiempo de duracion(seg) de vigencia de la cookie
        // setcookie("tuani", "this is a cookie very good",time()+30);

        // solo que lean los que se encuentran en este folder
        // setcookie("tuani", "this is a cookie very good",time()+30, "/Curso_PHP/cookie/");

        // borrar una cookie
        setcookie("tuani", "this is a cookie very good",time()-1, "/Curso_PHP/cookie/");
    ?>

    <h2>se acaba de guardar la cookie, probar cerrando el navegador y luego, volverlo a abrir</h2>
</body>
</html>