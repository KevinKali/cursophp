<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        if(isset($_COOKIE['idioma_seleccionado'])){
            if($_COOKIE['idioma_seleccionado'] == "es"){
                header('location:spanish.php');
            }else{
                header('location:english.php');
            }
        } 
    ?>
    <h1>Choose Idiom</h1>
    <div>
        <h3><a href="./crea_cookie.php?idioma=en">English</a></h3>
        <h4><a href="./crea_cookie.php?idioma=es">Spanish</a></h4>
    </div>
</body>
</html>