<?php require("reserva.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        session_start();
        if(!isset($_SESSION['user'])){
            header("location:index.html");
        }
    ?>  

    <?php 
        $booksJson = file_get_contents('./books.json');
        $books = json_decode($booksJson,true);

        if(isset($_GET['title'])){
            echo "<p>El libro seleccionado es: " . $_GET['title'] . "</p>";
            if(booking($books, $_GET['title'])){
                echo "El libro fue reservado <br>";
                update($books,$_GET['title']);
            }else{
                echo "El libro no esta disponible";
            }
        }else{
            echo "<p>Aun no ha reservado ninguno</p>";
        }
    ?>

    <ul>
        <?php foreach($books as $book): ?>
            <li>
                <a href="?title=<?php echo $book['title']; ?>">
                    <?php echo $book['title'] ?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
</body>
</html>