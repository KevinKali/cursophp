<?php

    function booking(array &$books, string $title): bool {
        foreach($books as $key=>$book){
            if($book['title']==$title){
                if($book['available']){
                    $books[$key]['available']=false;
                    return true;
                }else
                    return false;
            }
        }
        return true;
    }

    function update(array $books) {
        $booksJson = json_encode($books);
        file_put_contents(__DIR__ . "/books.json",$booksJson);
    }