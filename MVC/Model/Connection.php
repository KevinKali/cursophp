<?php

class Connect{
    public static function connection(){
        try{
            $con = new PDO("mysql:host=localhost; dbname=prueba","php","201299kei");
            $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $con->exec('SET CHARACTER SET utf8');
        }catch(Exception $e){
            die("error ".$e->getMessage());
        }

        return $con;
    }
}